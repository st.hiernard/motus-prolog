lire(Dictionnaire):-
    open('dicoComplet.txt',read,Str),
    lireMots(Str,Dictionnaire),
    close(Str).
    
lireMots(Stream,[]):- at_end_of_stream(Stream).    

lireMots(Stream,[Mot|L]):-
    \+at_end_of_stream(Stream),
    read(Stream,Mot),
    lireMots(Stream,L).