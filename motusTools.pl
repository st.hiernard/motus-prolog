%chargerDico(Mot). Charge le dictionnaire correspondant au Mot.
chargerDico(Mot):- Mot = [T|_],length(Mot,Taille), Taille < 10,
    atom_codes(L,[T]),
    number_atom(Taille,TailleAtom),
    append([d,i,c,o,s,p,l,/],[L,TailleAtom],PathChars),
    atom_chars(Path,PathChars),[Path].
chargerDico(Mot):- Mot = [T|_],length(Mot,Taille), Taille = 10,
    atom_codes(L,[T]),
    append([d,i,c,o,s,p,l,/],[L,'1','0'],PathChars),
    atom_chars(Path,PathChars),[Path].
    
%chargerDico(Lettre,Taille). Charge le dictionnaire correspondant au Mot.
chargerDico(Lettre,Taille):- 
    Taille < 10,
    number_atom(Taille,TailleAtom),
    append([d,i,c,o,s,p,l,/],[Lettre,TailleAtom],PathChars),
    atom_chars(Path,PathChars),[Path]. 
chargerDico(Lettre,Taille):- 
    Taille = 10,
    append([d,i,c,o,s,p,l,/],[Lettre,'1','0'],PathChars),
    atom_chars(Path,PathChars),[Path].
  
%getDico(Dico). Permet d''obtenir le dico sous forme de liste après l''avoir chargé.
getDico(Dico):-findall(X,dico(_,_,X),Dico).   

%afficherMot(Char_code_list). Affiche un mot a partir d''une liste sous forme [47,32,45].
afficherMot(Char_code_list):- atom_codes(Mot,Char_code_list),write(Mot).

%afficherElemsListe(Liste). Affiche tous les elements d''une liste.
afficherElemsListe([]).
afficherElemsListe([T|Q]):- write(T),afficherElemsListe(Q). 

%remplacerA(Liste,Position,Element,ResultatListe). Remplace l''element de la liste par un nouvel element a une certaine position. Démarre à 0.
remplacerA([_|T],0,X,[X|T]).
remplacerA([H|T],I,X,[H|R]):- I > -1, NI is I-1, remplacerA(T,NI,X,R),!.
remplacerA(L,_,_,L).

%Un mot est choisi aloéatoirement depuis le dictionnaire
choisiMot(Mot):- getRandomDico(D), randomize,length(D,Taille),Max is Taille+1,random(1,Max,N), nth(N,D,Mot). 

%Choisi une letre au hasard
choisiLettre(Lettre):- randomize, random(65,91,CharCode), char_code(Lettre,CharCode).

%choisi une taille de mot au hasard entre 5 et 10.
choisiTaille(Taille):- randomize, random(5,11,Taille).

%Obtient un dico aléatoirement.
getRandomDico(Dico):- choisiLettre(Lettre),choisiTaille(Taille),chargerDico(Lettre,Taille),getDico(Dico).

%initNombreTours(Mot,NbTours). Le nombre de tours vaut 5 si le mot est de taille 5, sinon 6.
initNombreTours(Mot,5):- length(Mot,Taille), Taille is 5.
initNombreTours(_,6).

%initAffichage(Mot,Sortie). Mot est de la forme [47,32,45]
initAffichage(Mot,Sortie):- initAffichage1(Mot,X),initAffichage2(Mot,X,Sortie).
initAffichage1([],[]).
%initAffichage1(Mot,Sortie). Remplie Sortie avec des '.'.
initAffichage1([_|Q1],[.|Q2]):-initAffichage1(Q1,Q2).
%initAffichage2(Mot,Sortie). Remplace le 1er caractere de Sortie par la premiere lettre du mot.
initAffichage2([T|_],AffichageInit,Sortie):- 
    atom_codes(X,[T]),
    remplacerA(AffichageInit,0,X,Sortie).

%conditionVictoire(Mot,Victoire).Genere un mot victoire rempli d''autant de ! que de lettres du mot
conditionVictoire([],[]).
conditionVictoire([_|Q1],[!|Q2]):-conditionVictoire(Q1,Q2).

% Verifie si le mot et la saisie ont la meme taille
testTailleEntree(Mot,Entree):- 
    length(Mot,Taille1),length(Entree,Taille2),Taille1=Taille2,nl,!.
testTailleEntree(Mot,Entree):- 
    write('Le mot saisi doit etre de taille '), 
    length(Mot,Taille1), write(Taille1),
    write(' et non pas '),length(Entree,Taille2),write(Taille2),nl,fail.

%upper(Atom,Upper). Rend atom en majuscule.
upper(Atom,Upper):- atom_chars(Atom,List),upperList(List,UpperList),atom_chars(Upper,UpperList).
upperList([],[]).
upperList([T1|Q1],[U|Q2]):- lower_upper(T1,U), upperList(Q1,Q2).

%misAJourAffichage(Mot,LettresBienPlacees,AncienAffichage,Sortie).Affiche les lettres où il y a un ! dans Sortie.
misAJourAffichage([],[],[],[]).
misAJourAffichage([T|Q1],[!|Q2],[_|Q3],[T|Q4]):-misAJourAffichage(Q1,Q2,Q3,Q4).
misAJourAffichage([_|Q1],[T|Q2],[.|Q3],[.|Q4]):- T \= !,misAJourAffichage(Q1,Q2,Q3,Q4).
misAJourAffichage([_|Q1],[T|Q2],[L|Q3],[L|Q4]):- T \= !,L \= '.',misAJourAffichage(Q1,Q2,Q3,Q4).

%verifieLettres(Lettres,Entree,AffichageInit,AffichageSortie). Génère une sortie qui indique les lettres bien placées('!') et mal placées ('?'). 
verifieLettres(Lettres,Entree,AffichageInit,NouvelleSortie):-
    verifieLettresBienPlacees(Lettres,Entree,AffichageInit,0,AffichageSortie),
    remplaceLettresBienPlacees(Lettres,AffichageSortie,NouvellesLettres),
    verifieLettresMalPlacees(NouvellesLettres,Entree,AffichageSortie,0,NouvelleSortie).   
        
%verifieLettresBienPlacees(Lettres,Entree,AffichageInit,N,AffichageSortie). Ajoute des ! pour les lettres bien placées : Ex : [!,!,!,'.',!]
verifieLettresBienPlacees([],[],L,_,L).
verifieLettresBienPlacees([TL|QL],[TL|QE],AffichageInit,NB,AffichageSortie):-
        remplacerA(AffichageInit,NB,!,Res),
        N is NB+1,
        verifieLettresBienPlacees(QL,QE,Res,N,AffichageSortie),!.
verifieLettresBienPlacees([TL|QL],[TE|QE],AffichageInit,NB,AffichageSortie):-
    TL \= TE,
    N is NB+1,
    verifieLettresBienPlacees(QL,QE,AffichageInit,N,AffichageSortie),!.    
       

%remplaceLettresBienPlacees(Mot,Placements,Sortie). Remplace les lettres bien placées par des 0 dans Sortie.
remplaceLettresBienPlacees([],[],[]).
remplaceLettresBienPlacees([_|Q1],[!|Q2],[0|Q3]):- 
    remplaceLettresBienPlacees(Q1,Q2,Q3).
remplaceLettresBienPlacees([H|Q1],[T|Q2],[H|Q3]):- 
    T \= '!',     
    remplaceLettresBienPlacees(Q1,Q2,Q3).
    
%test : remplaceLettresBienPlacees("PLUIE",[!,.,!,.,.],X).  


%verifieLettresMalPlacees(Lettres,Entree,AffichageInit,N,AffichageSortie). Ajoute des ? pour les lettres mal placées : Ex : [?,?,'.','.',?]
%Cas d''arret
verifieLettresMalPlacees(_,[],L,_,L).
%Cas où il n''y a pas de lettre bien placée à la position N, mais mal placée.
verifieLettresMalPlacees(Lettres,[TE|QE],AffichageInit,NB,AffichageSortie):-
        NB3 is NB+1,
        nth(NB3,Lettres,TL),
        TL \= 0,
        memberchk(TE,Lettres),
        nth(NB2,Lettres,TE),
        NB4 is NB2-1,
        remplacerA(Lettres,NB4,1,NouvellesLettres),
        remplacerA(AffichageInit,NB,?,Res),
        N is NB+1,
        verifieLettresMalPlacees(NouvellesLettres,QE,Res,N,AffichageSortie),!.
%Cas où il n''y a ni lettre bien placée à la position N, ni mal placée.
verifieLettresMalPlacees(Lettres,[TE|QE],AffichageInit,NB,AffichageSortie):-
    \+memberchk(TE,Lettres),
    N is NB+1,
    verifieLettresMalPlacees(Lettres,QE,AffichageInit,N,AffichageSortie),!.  
%Cas où il y a une lettre bien placée à la position N, et que la lettre est contenu dans Lettres.
verifieLettresMalPlacees(Lettres,[TE|QE],AffichageInit,NB,AffichageSortie):-
    memberchk(TE,Lettres),
    NB3 is NB+1,
    nth(NB3,Lettres,TL),
    TL = 0,
    N is NB+1,
    verifieLettresMalPlacees(Lettres,QE,AffichageInit,N,AffichageSortie),!.     


%Ex pour vérifier: verifieLettres([b,a,r,b,e],[b,a,r,r,e],[b,.,.,.,.],X).
%Ex pour vérifier: verifieLettres([b,a,r,i,t,o,n],[b,a,r,n,u,e,t],[b,.,.,.,.,.,.],X).
%Ex pour vérifier: verifieLettres([m,i,r,o,i,r],[m,i,r,i,m,o],[.,.,.,.,.,.],X).
%Ex pour vérifier: verifieLettres([l,i,v,r,e,t],[l,i,r,v,t,r],[.,.,.,.,.,.],X).
%Ex pour vérifier: verifieLettres([u,n,i,o,n],[u,p,i,l,n],[.,.,.,.,.],X).
%Ex pour vérifier: verifieLettres([l,i,v,r,e,t],[l,i,e,r,t,v],[.,.,.,.,.,.],X).



%PARTIE IA

%estDansDico(Element). Verifie si l''element est dans le dico chargé. On se servira d''elements du type [80,76,_,73,_] pour matcher avec les mots du dico.
estDansDico(Element):- getDico(D),member(Element,D),!.
estDansDico(Element):- afficherMot(Element),write(' n''existe pas dans le dictionnaire'),nl,fail.

%estDansDico(Element,Dico). Verifie si l''element est dans le dico en parametre. On se servira d''elements du type [80,76,_,73,_] pour matcher avec les mots du dico.
estDansDico(Element,Dico):- member(Element,Dico).

%getAllFromDico(Element,Dico,List). Retourne une liste des mots qui matchent avec l''element du type [80,76,_,73,_].
getAllFromDico(Element,Dico,List):- findall(Element,estDansDico(Element,Dico),List).

%Ex : | ?- getDico(D),getAllFromDico([87,72,_,_,_],D,Z).
%Z = [[87,72,65,82,70],[87,72,73,71,83],[87,72,73,80,83],[87,72,73,83,84]]

%getPatternLettresBienPlacees(Mot,Placements,Pattern). Crée un pattern a partir du mot et des lettres bien placées.
getPatternLettresBienPlacees([],[],[]).
getPatternLettresBienPlacees([T|Q1],[!|Q2],[T|Q3]):- getPatternLettresBienPlacees(Q1,Q2,Q3).
getPatternLettresBienPlacees([_|Q1],[T|Q2],[_|Q3]):- T \= !, getPatternLettresBienPlacees(Q1,Q2,Q3).

%Ex: | ?- getPatternLettresBienPlacees("PLUIE",['!','!','.','!','.'],Z).
%Z = [80,76,_,73,_] ? 
%On pourra le reutiliser dans getAllFromDico pour garderles mots ayant les lettres bien placées.

%motsAvecLettresBienPlacees(Lettres,Placement,Dico,ListeMots). Obtient la liste des mots qui on les lettres bien placées.
motsAvecLettresBienPlacees(Lettres,Placement,Dico,ListeMots):-
    getPatternLettresBienPlacees(Lettres,Placement,Pattern),getAllFromDico(Pattern,Dico,ListeMots).

%Ex : | ?- getDico(D),motsAvecLettresBienPlacees("PLUIE",['!','!','.','!','.'],D,Z).                      
% Z = [[80,76,65,73,68],[80,76,65,73,69],[80,76,65,73,78],[80,76,65,73,83],[80,76,65,73,84],[80,76,69,73,78],[80,76,79,73,69],[80,76,85,73,69]] ? 


%motsAvecLettresMalPlacees(Lettres,Placement,Dico,Mot,ListeMots,N). Obtient la liste des mots qui on les lettres mal placées. 
motsAvecLettresMalPlacees([],[],Dico,_,Dico,_).
% On enleve les mots du dico qui ont la lettre à cet endroit.
motsAvecLettresMalPlacees([T|Q1],['?'|Q2],Dico,Mot,ListeMots,N):-
    NB is N+1,
    motsContenantLaLettre(Dico,T,NewListeMots),
    getPatternLettreAvecPosition(Mot,T,N,Pattern),
    enleveMotsContenantLePattern(NewListeMots,Pattern,NewListeMots2),
    motsAvecLettresMalPlacees(Q1,Q2,NewListeMots2,Mot,ListeMots,NB).
motsAvecLettresMalPlacees([_|Q1],[T|Q2],Dico,Mot,ListeMots,N):-
    NB is N+1,
    T \= ('?'), motsAvecLettresMalPlacees(Q1,Q2,Dico,Mot,ListeMots,NB).

% chargerDico('X',6).
%Ex : | ?- getDico(D),motsAvecLettresMalPlacees([88,89,76,69,77,69],['!','!','.','!','.','?'],D,[88,89,76,69,77,69],Z,0).
%D = [[88,69,78,79,78,83],[88,72,79,83,65,83],[88,73,65,78,71,83],[88,73,80,72,79,83],[88,89,76,69,77,69],[88,89,76,69,78,69],[88,89,76,79,76,83],[88,89,76,79,83,69],[88,89,83,84,69,83]]
%Z = [[88,69,78,79,78,83],[88,89,83,84,69,83]] ? 


%motsContenantLaLettre(Dico,Lettre,ListeMots). Lettre sous forme de code. Reourne la liste des mots qui contiennent la lettre.
motsContenantLaLettre([],_,[]).
motsContenantLaLettre([Mot|Q1],Lettre,[Mot|Q2]):- 
    memberchk(Lettre,Mot),
    motsContenantLaLettre(Q1,Lettre,Q2).
motsContenantLaLettre([Mot|Q1],Lettre,ListeMots):-
    \+ memberchk(Lettre,Mot),
    motsContenantLaLettre(Q1,Lettre,ListeMots).
    
%enleveMotsAvecLettresAbsentes(Entree,Placement,Dico,Sortie). Enleve les lettres du mots avec des '.' et gèrent les cas particuliers.
enleveMotsAvecLettresAbsentes(Entree,Placement,Dico,Sortie):-                   
    enleveMotsAvecLettresAbsentesP(Entree,Placement,Entree,Placement,Dico,Sortie,0).
    
%Cas d''arret
enleveMotsAvecLettresAbsentesP([],[],_,_,Dico,Dico,_).
%Cas '.' et pas de '!' ou '?' sur une même lettre  précédente
enleveMotsAvecLettresAbsentesP([T|Q1],['.'|Q2],Entree,Placement,Dico,Sortie,N):-
    \+ isLettreBienOuMalPlacee(Entree,Placement,T),
    enleveMotsContenantLaLettre(Dico,T,NewDico),
    NB is N+1,
    enleveMotsAvecLettresAbsentesP(Q1,Q2,Entree,Placement,NewDico,Sortie,NB).
%Cas '.' et et présence de '!' ou '?' sur une même lettre précédente. On enleve les mots du dico qui ont la lettre à cet endroit.
enleveMotsAvecLettresAbsentesP([T|Q1],['.'|Q2],Entree,Placement,Dico,Sortie,N):-
    isLettreBienOuMalPlacee(Entree,Placement,T),
    getPatternLettreAvecPosition(Entree,T,N,Pattern),
    enleveMotsContenantLePattern(Dico,Pattern,NewDico),
    NB is N+1,
    enleveMotsAvecLettresAbsentesP(Q1,Q2,Entree,Placement,NewDico,Sortie,NB).
%Cas de '!' ou '?' sur la lettre
enleveMotsAvecLettresAbsentesP([_|Q1],[T2|Q2],Entree,Placement,Dico,Sortie,N):- 
    T2 \= '.',  
    NB is N+1,
    enleveMotsAvecLettresAbsentesP(Q1,Q2,Entree,Placement,Dico,Sortie,NB).
    
%Ex: chargerDico('X',5).
%| ?- getDico(D),enleveMotsAvecLettresAbsentes([88,69,82,69,83],['!','.','?','.','?'],D,Sortie).    
%D = [[88,69,78,79,78],[88,69,82,69,83],[88,69,82,85,83],[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76],[88,89,83,84,69]]
%Sortie = [[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76]] ? 

%| ?- getDico(D),enleveMotsAvecLettresAbsentes([88,69,82,73,83],['!','.','?','.','?'],D,Sortie).
%D = [[88,69,78,79,78],[88,69,82,69,83],[88,69,82,85,83],[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76],[88,89,83,84,69]]
%Sortie = [[88,72,79,83,65],[88,89,76,79,76]] ? 

%Ex : getDico(D),enleveMotsAvecLettresAbsentes([88,69,79,69,73],['!','?','.','.','?'],D,Sortie).
%| ?- getDico(D),enleveMotsAvecLettresAbsentes([88,69,79,69,73],['!','?','.','.','?'],D,Sortie).
%D = [[88,69,78,79,78],[88,69,82,69,83],[88,69,82,85,83],[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76],[88,89,83,84,69]]
%Sortie = [[88,69,82,69,83],[88,69,82,85,83],[88,73,65,78,71],[88,89,83,84,69]] ? 

%Ex où on traite cas particulier : 
%| ?- getDico(D),enleveMotsAvecLettresAbsentes([88,69,82,69,83],['!','?','?','.','?'],D,Sortie).
%%D = [[88,69,78,79,78],[88,69,82,69,83],[88,69,82,85,83],[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76],[88,89,83,84,69]]
%Sortie = [[88,69,78,79,78],[88,69,82,85,83],[88,72,79,83,65],[88,73,65,78,71],[88,73,80,72,79],[88,89,76,79,76],[88,89,83,84,69]] ? 

%enleveMotsContenantLaLettre(Dico,Lettre,Sortie). Enleve les mots du dico qui contiennent la lettre en parametre.
enleveMotsContenantLaLettre([],_,[]).
enleveMotsContenantLaLettre([T|Q],Lettre,[T|R]):- \+memberchk(Lettre,T), enleveMotsContenantLaLettre(Q,Lettre,R).
enleveMotsContenantLaLettre([T|Q],Lettre,R):- memberchk(Lettre,T), enleveMotsContenantLaLettre(Q,Lettre,R).

%Ex : getDico(D),enleveMotsContenantLaLettre(D,85,Sortie).

%isLettreBienOuMalPlacee(Mot,Placement,Lettre). Dis si la lettre en parametre est mal placéee ou bien placée.
isLettreBienOuMalPlacee([Lettre|_],['?'|_],Lettre):-!.
isLettreBienOuMalPlacee([Lettre|_],['!'|_],Lettre):-!.
isLettreBienOuMalPlacee([_|Q1],[_|Q2],Lettre):- isLettreBienOuMalPlacee(Q1,Q2,Lettre).

%Ex: isLettreBienOuMalPlacee("PLUIE",['!','.','?','.','?'],85).
%Ex: isLettreBienOuMalPlacee("PLUIE",['!','.','!','.','?'],85).
%Ex: isLettreBienOuMalPlacee("PLUIE",['!','.','.','.','?'],85).

%enleveMotsContenantLePattern(Dico,Pattern,Sortie). Eneleve les lettres du dico qui contiennent une certaine lettre.
%Ex du pattern pour un mo de 5 lettres : [_,69,_,_,_]
enleveMotsContenantLePattern([],_,[]).
enleveMotsContenantLePattern([T|Q],Pattern,[T|R]):- T \= Pattern, enleveMotsContenantLePattern(Q,Pattern,R).
enleveMotsContenantLePattern([_|Q],Pattern,R):- enleveMotsContenantLePattern(Q,Pattern,R).

%Ex : getDico(D),enleveMotsContenantLePattern(D,[_,69,_,_,_],Sortie).

%getPatternLettreAvecPosition(Mot,Lettre,Position,Pattern). Renvoie un pattern du style [_,69,_,_,_], utilisable pour le predicat enleveMotsContenantLePattern(Dico,Pattern,Sortie).
%Rempli un pattern avec des '_' d''abord, puis ajoute la lettre a la fin.
getPatternLettreAvecPosition1([],[]).
getPatternLettreAvecPosition1([_|Q1],[_|Q2]):- getPatternLettreAvecPosition1(Q1,Q2).
getPatternLettreAvecPosition(Mot,Lettre,Position,Pattern):- 
    getPatternLettreAvecPosition1(Mot,PatternInter),
    remplacerA(PatternInter,Position,Lettre,Pattern).

%Ex : | ?- getPatternLettreAvecPosition([80,76,85,73,69],85,2,P).
%P = [_,_,85,_,_]

%reduireDico(Lettres,Placement,Entree,Dico,Sortie). Reduit le dico de l''IA en utilsant les placements de lettres.
reduireDico(Lettres,Placement,Entree,Dico,Sortie):- 
    motsAvecLettresBienPlacees(Lettres,Placement,Dico,ListeMots),
    motsAvecLettresMalPlacees(Entree,Placement,ListeMots,Entree,DicoReduit,0),
    enleveMotsAvecLettresAbsentes(Entree,Placement,DicoReduit,Sortie).
    
%Ex :  | ?- getDico(D),reduireDico([88,89,76,69,77,69],['!','!','?','!','.','?'],D,Z). 
%D = [[88,69,78,79,78,83],[88,72,79,83,65,83],[88,73,65,78,71,83],[88,73,80,72,79,83],[88,89,76,69,77,69],[88,89,76,69,78,69],[88,89,76,79,76,83],[88,89,76,79,83,69],[88,89,83,84,69,83]]
%Z = [[88,89,76,69,77,69],[88,89,76,69,78,69]] ? 
    
    
%choisiMot(Mot,Dico). Un mot est choisi aloéatoirement depuis le dictionnaire en parametre.
choisiMot(Mot,Dico):- randomize,length(Dico,Taille),Max is Taille+1,random(1,Max,N), nth(N,Dico,Mot).   
      
% Verifie si la saisie est entre 5 et 10 caracteres.
testTailleEntree(Entree):- 
    length(Entree,Taille),Taille > 4,Taille < 11,nl,!.
testTailleEntree(_):- 
    write('Le mot saisi doit avoir entre 5 et 10 caracteres'),nl,fail.   
    

%debugDico(Dico).Affiche tous les elements du dico.
debugDico1([]).
debugDico1([T|Q]):- afficherMot(T), write(' '),debugDico1(Q).
debugDico(Dico):- write('Dictionnaire de l''ordinateur : ['),debugDico1(Dico),write(']').

