
%Menu

motus:- [motusTools],repeat, menu, !.

menu:- nl, write('1. Deviner un mot choisi par l''ordinateur'),nl,
write('2. Faire deviner un mot à l''ordinateur'),nl,
write('3. Terminer'),nl,
write('Entrer un choix : '),
read(Choix),nl, appel(Choix),nl,
Choix=3, nl.

appel(1):- write('Vous avez choisi 1.'),nl,menu1,!.
appel(2):- write('Vous avez choisi 2.'),nl,menu2,!.
appel(3):- write('Au revoir'),!.
appel(_):- write('Vous avez mal choisi').



%Gestion du motus

%menu1 : deviner un mot choisi par l''ordi

menu1:- choisiMot(Mot),jouer1(Mot).

debug(Mot):- length(Mot,Taille),afficherMot(Mot),write(' '),write(Taille),nl.

jouer1(Mot):- 
    initAffichage(Mot,Affichage),conditionVictoire(Mot,Victoire),
    initNombreTours(Mot,NbTours),
    nl, afficherElemsListe(Affichage),
    tour1(Mot,Victoire,NbTours,Affichage),!.
   

%La proposition est traduite en liste de codes, et sa taille est testée.
%Puis on vérifie les éléments bien placés et mal placés.

%la fonction var(Term) teste si Fini a tété instanciée. Si oui, c''est que le joueur a gagné. Donc ca fail e ca va au dernier cas de tour1 on cela écrit que le joueur gagne. 

tour1(Mot,Victoire,N,AncienAffichage):-
    N > 0,
    nl, write('Tours restants : '), write(N), nl,
    demandeProposition(Mot,Entree),
    initAffichage1(Mot,MotAvecQueDesPoints),
    verifieLettres(Mot,Entree,MotAvecQueDesPoints,Sortie),
    afficherElemsListe(Sortie),nl,nl,
    atom_codes(MotDecode,Mot), atom_chars(MotDecode,MotDecodeListe),
    misAJourAffichage(MotDecodeListe,Sortie,AncienAffichage,NouvelAffichage),
    afficherElemsListe(NouvelAffichage),
    NB is N - 1,
    partieGagnee(Sortie,Victoire,Fini),
    var(Fini),
    tour1(Mot,Victoire,NB,NouvelAffichage),!.
tour1(Mot,_,N,_):- N < 1,nl,nl,write('Perdu, vous n''avez plus de tours restants! Le mot etait : '),
    afficherMot(Mot),!.  
tour1(_,_,_,_):- nl,nl,write('Bravo!'),!.

%partieGagnee(Sortie,Victoire,Fini). Instancie Fini si Sortie = Victoire
partieGagnee(Victoire,Victoire,1).
partieGagnee(Sortie,Victoire,_):- Sortie \= Victoire.

demandeProposition(Mot,Entree):-
    repeat,
    write('Votre Proposition : '), 
    read(Proposition),
    upper(Proposition,Upper),
    atom_codes(Upper,Entree),
    testTailleEntree(Mot,Entree),
    write(Upper),nl,!.


%menu2 : Faire deviner un mot à l''ordinateur

menu2:- demandeMot(Mot),jouer2(Mot).

demandeMot(Entree):-
    repeat,
    write('Saisissez un mot (entre 5 et 10 lettres) : '), 
    read(Proposition),
    upper(Proposition,Upper),
    atom_codes(Upper,Entree),
    testTailleEntree(Entree),
    chargerDico(Entree),
    estDansDico(Entree),
    write(Upper),nl,!.

jouer2(Mot):-
    initAffichage(Mot,Affichage),conditionVictoire(Mot,Victoire),
    initNombreTours(Mot,NbTours),
    nl, afficherElemsListe(Affichage),
    getDico(Dico),
    tour2(Mot,Victoire,NbTours,Affichage,Dico),!.

tour2(Mot,Victoire,N,AncienAffichage,Dico):-
    N > 0,
%    nl,debugDico(Dico),
    nl, write('Tours restants : '), write(N), nl,
    choisiMot(Entree,Dico),
    afficherMot(Entree),nl,
    delete(Dico,Entree,DicoSansMot),
    initAffichage1(Mot,MotAvecQueDesPoints),
    verifieLettres(Mot,Entree,MotAvecQueDesPoints,Sortie),
    afficherElemsListe(Sortie),nl,nl,
    atom_codes(MotDecode,Mot), atom_chars(MotDecode,MotDecodeListe),
    misAJourAffichage(MotDecodeListe,Sortie,AncienAffichage,NouvelAffichage),
    afficherElemsListe(NouvelAffichage),
    NB is N - 1,
    partieGagnee(Sortie,Victoire,Fini),
    var(Fini),
    reduireDico(Mot,Sortie,Entree,DicoSansMot,NewDico),
    tour2(Mot,Victoire,NB,NouvelAffichage,NewDico),!.
tour2(Mot,_,N,_,_):- N < 1,nl,nl,write('L''ordinateur a perdu, il n''y a plus de tours restants! Le mot etait : '),
    afficherMot(Mot),!.  
tour2(_,_,_,_,_):- nl,nl,write('L''ordinateur a reussi !'),!.

