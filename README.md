# Projet IA02 - Motus - UTC

Créateur :

- HIERNARD Stéphane


## Objectif

Créer un programme de motus en GNUProlog.

Faire deviner un mot à l'IA ou deviner un mot proposé par l'IA.

## Fichiers nécessaires

- motus.pl
- motusTools.pl
- dicospl/

## Instructions

- Installer GNUProlog.
- Compiler motus.pl (double click sur Windows | [motus] dans un bash).
- Exécuter :
```
motus.
```
- Suivez les instructions.
- Lors d'une saisie, écrivez en minuscule et valider avec un '.' à la fin avant de faire "Entrée".
Ex :
```
1.
caramel.
tournesol.
```
